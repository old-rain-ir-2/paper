/// This function doubles the value of it's argument
fn double_vec(v: Vec<u8>) -> Vec<u8>;

/// This function returns some slice of the underlying vector
fn slice_vec(v: &Vec<u8>) -> &[u8];

/// This function hashes it's argument
fn hash(s: &[u8]) -> u64;

fn example_1(v: Vec<u8>) -> (Vec<u8>, u64) {
    let s: &[u8] = slice_vec(&v);
    let h: u64 = hash(s);
    let u = double_vec(v);
    (u, c)
}
