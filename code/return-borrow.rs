fn return_borrow_good(x: u64) -> &u64 {
    // Here, the free variable set of the borrow, {x}, intersects 
    // with the argument set of the function
    &x
}

fn return_borrow_good(_x: u64) -> &u64 {
    // The value returned is a constant with respect to the function, 
    // so this is fine
    &5
}

fn return_borrow_closure(x: u64) -> u64 {
    // This closure is OK since the free variable set of the returned 
    // reference, {x}, does not intersect the argument set of the 
    // closure, {y}
    let f = |_y| &x;
    *f(x)    
}