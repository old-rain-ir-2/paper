# The `rain` Intermediate Representation

This document contains the formal description and documentation of the `rain` intermediate representation in LaTeX, as well as various auxiliary design documents.