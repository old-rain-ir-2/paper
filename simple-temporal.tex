\documentclass{article}
\usepackage[margin=1.5in]{geometry}
\usepackage{rain}
\addbibresource{references.bib}
%\setcounter{tocdepth}{2}

\title{An Informal Introduction to Temporal Type Theory}
\author{Jad Ghalayini and Qingyuan Qie}

\begin{document}

\maketitle

\begin{center}
    \textbf{Note:} this paper is a very early draft of our attempts to write up the work we have done on the \rain project \cites{rain-ir}{rain-lang} over the summer, which gave rise to the idea of temporal dependent types. In particular, the final sections are not complete, and we plan to add much more information on e.g. the nontermination monad.
\end{center}

\tableofcontents

\newpage

\section{Introduction}

Dependent type theory and substructural logic are two of the most influential ideas in modern computer science research. Recent work, such as \cites{lindep}{lindep-syn}{quantumlinear}, has allowed us to integrate linear types, which encode a substructural logic, with dependent types. By providing fine-grained control over the flow of data within a proof, substructural logics allow us to obtain a handle on the notion of a ``resource,'' and hence have found many applications in modeling programs relying on destructive updates, from concurrent processes \cite{csl} to quantum ones \cites{quantumlinear}{quantalg}. By integrating separation logic with dependent types, we can bring the power of the Curry-Howard correspondence to bear on problems in these settings \cites{lindep}{quantumlinear}.

Outside the world of dependent type theory, one of the most promising applications of sub-structural logic in programming language design is the Rust programming language's system of \textit{lifetimes} \cites{rust}{rustbelt}{stackedborrows}. By extending an affine type system with a concept of ``borrowing,'' Rust allows the programmer to perform complex, machine-checked resource management without the use of a garbage collector, allowing C-like performance without the risk of memory leaks or data races \cites{rust}{rustbelt}{stackedborrows}. Unfortunately, for some low-level tasks and primitives, Rust programmers must drop down to ``\tty{unsafe}" code, which allows the manipulation of raw pointers but, consequently, comes with all the risks of C \cites{rust}{rustbelt}{stackedborrows}. The RustBelt project (\cite{rustbelt}) has focused on proving the soundness of Rust's guarantees; namely, that if all \tty{unsafe} code in a program is Undefined Behaviour (UB) free, then the entire program is so as well, while the Stacked Borrows project (\cite{stackedborrows}) has focused on defining a formal semantics for the lifetime system itself.

In this report, we propose a system of \textit{temporal dependent types}, which we define to be sub-structural dependent types \textit{extended} with a Rust-like notion of \textit{lifetimes}. The \rain project \cites{rain-ir}{rain-lang} is currently working towards an intermediate representation having an informally-specified version of such a type system. We hope to, much like the Rust language itself, allow the natural expression and verification of programs performing low-level resource manipulation with minimal language or runtime overhead. In particular, \rain does not require the usage of a garbage collector or any complex runtime and targets (currently) Cranelift IR, which we may lower further into a machine-specific assembly language of choice.

One exciting application we have in mind is the application of dependent types to the \textit{internal} verification of fragments of \tty{unsafe} code in an otherwise safe program. As proposed by the RustBelt project \cite{rustbelt}, by proving only the \tty{unsafe} modules in a complex program memory-safe, we can deduce that the entire program must be memory safe. In a language supporting temporal dependent types, we can do this within the language itself, without requiring external tools. One immediate application of this property is to allow precisely establishing the Trusted Computing Base of a given application, and even allow, e.g., securely executing un-trusted applications from the Internet written in a systems language, versus a sandboxed language like JavaScript or WebAssembly. Another exciting prospect is the gradual verification of a library's specifications (beyond just memory safety) by introducing, for example, dependently typed propositions asserting the library's functions behave as expected.

A particularly interesting concept in our current implementation of temporal dependent types is the distinction between ``abstract types'' (or just types) and ``concrete types'' or (``representations''). In particular, we say every concrete type is an instance of a unique abstract type: for example, the types \tty{\&'a T} (a reference to \(T\) which is valid for lifetime \(a\)), \tty{\&'a mut T} (a unique reference to \(T\) which is valid for lifetime \(a\)), and \tty{register T} (a value of type \(T\) stored in a ``virtual register") all share the same abstract type, namely \(T\). Only concrete types may be substructural/temporal, and only terms of abstract type may appear in dependent types (with every term of concrete type coercing to a term of abstract type): this, as in \cite{lindep}, side-steps the problems with substructural/temporal terms appearing in types, but also means that we may vary the system of dependent types underlying our temporal type system. Of particular interest, then, is the potential for higher inductive types, and in particular, univalent types (as in \cite{hottbook}), to be used as the underlying system for temporal types, with higher inductive/univalent linear type systems already showing promise in quantum theory and theoretical physics \cite{homotopy-lin}.

In this first paper, we will go over the basic idea of a temporal type system, which we will develop as an extension of the simply typed lambda calculus, and sketch how it can be extended to a dependent type system. We hope to follow this up with a more rigorous treatment of temporal dependent types, and a detailed technical report on their implementation in the \rain project.

\section{Introduction to the Temporal RVSDG}

\label{sec:rvsdg}

We will base our theory off a heavily simplified Regionalized Value State Dependence Graph (RVSDG) \cite{rvsdg}; a novel intermediate representation (IR) which represents imperative, systems-level programs as functional programs subject to sub-structurality conditions. Our modifications can be described as twofold: annotating the RVSDG with dependent types and annotating the RVSDG with lifetimes. While these are two somewhat orthogonal changes, they interact in very interesting ways (e.g., in the presence of branching), and we believe that the resulting type theory is interesting in it's own right: this is the theory of temporal dependent types we are investigating. Note we will be working with a very simplified notion of an RVSDG, which we will take to be a data-dependence graph extended with ``state edges'' (for our purposes, sub-structural edges) and ``regions'' in which parameters may be used. Throughout this paper, we will represent RVSDG-based programs by a Rust-like pseudocode, as well as in a simple graphical format representing these RVSDGs. We, consequently, will begin with an example.

\begin{figure}[h]
    \centering
    \lstinputlisting[language=rust, style=rainstyle]{code/example1.rs}
    \caption{
        A simple Rust program which consumes a vector, and returns a hash of some slice of that vector and the vector back, doubled.
        \label{fig:ex1}
    }
\end{figure}

Consider the simple Rust program in Figure \ref{fig:ex1}, which consumes a vector, and returns a hash of some slice of that vector and the vector, doubled. Writing this out as a data dependence graph, we obtain Figure \ref{fig:ex1-data}: we will draw edges which consume their argument, which we will call \textbf{state edges}, in \textbf{bold}: this represents a sub-structural (affine or linear) resource which can be used at most once. Recall that a sub-structural type can be \textbf{affine} (can be used at most once), \textbf{relevant} (must be used at least once), or \textbf{linear} (affine and relevant, i.e., must be used exactly once). As an example, the code in \ref{fig:code-doubleuse}, corresponding to the graph in Figure \ref{fig:graph-doubleuse}, as it contains a double-use bug, i.e. two state edges going out of an affine type.

The issue with the unaugmented graph in Figure \ref{fig:ex1-data} is that, while \(h\) does not depend on \(u\), if we tried to reconstruct a program from the graph, we could also get the \textit{incorrect} programs in Figure \ref{fig:ex1-bad}. So, we want to augment the data dependence graph with the necessary information such that we will never reconstruct an invalid graph: we will add \textit{temporal dependencies} represented as dotted lines.

\begin{figure}
    \centering
    \tikzfig{graphs/example1-plain}
    \caption{
        A data-dependence graph for the program in Figure \ref{fig:ex1}
        \label{fig:ex1-data}
    }
\end{figure}

\begin{figure}
    \centering
    \lstinputlisting[language=rust, style=rainstyle]{code/double-use.rs}
    \caption{
        A program demonstrating a simple double-use error: \(v\) is used first in the construction of \(u\), and then again in the return value, even though it is affine.
    }
    \label{fig:code-doubleuse}
\end{figure}

\begin{figure}
    \centering
    \tikzfig{graphs/double-use}
    \caption{
        An RVSDG for the program in Figure \ref{fig:code-doubleuse}: in this graphical format, the double-use error is clearly visible as two state edges exiting \(v\).
        \label{fig:graph-doubleuse}
    }
\end{figure}

\begin{figure}
    \centering
    \lstinputlisting[language=rust, style=rainstyle]{code/example1-bad.rs}
    \caption{
        Invalid instruction re-orderings for the program in Figure \ref{fig:ex1}
        \label{fig:ex1-bad}
    }
\end{figure}

We know that \(s\), and \(\&v\), both borrow from \(v\) and hence cannot be used once \(v\) is destroyed, which happens when \(u\) is created, so, we will draw dotted lines from \(s\) and \(\&v\) to \(u\). Note, however, that this also induces an implict constraint: while \(h\) does not borrow from \(v\), and hence can be used after \(v\) is destroyed, it must still be \textit{computed} before \(v\) is destroyed: we will hence add a dotted line from \(h\) to \(u\): this is the graph in \ref{fig:ex1-constrained}. While the graph is now correct, it is still not clear how to algorithmically deduce these temporal dependencies purely from the type theory, and they are not as informative as they could be: \(u\) does not come after \(h\) the same way it comes before \(s\) and \(\&v\), since it comes after any usages of \(s, \&v\) but only after the \textit{creation} of \(h\).

\begin{figure}
    \centering
    \tikzfig{graphs/example1-constrained}
    \caption{
        A data-dependence graph for the program in Figure \ref{fig:ex1}, annotated with temporal dependencies to prevent the invalid orderings in Figure \ref{fig:ex1-bad}.
        \label{fig:ex1-constrained}
    }
\end{figure}

We hence simultaneously generalize and simplify our model by assigning to each node in the data dependence graph a \textit{lifetime}. Each lifetime consists of two instants: a \textit{beginning} and an \textit{end}. These instants live in a DAG of instants (as in concurrent programming, we do \textit{not} assume sequential consistency) which forms a partial order strictly stronger than that of the raw data dependence DAG. We then assert that every node happens after it's beginning and before it's end, and that every \textit{access} of the node must occur within this range. In our graphical notation, we annotate the nodes of the data dependence graph with parentheses, as in Figure \ref{fig:ex1-graph}. An opening parenthesis denotes the beginning of a lifetime, and a closing parenthesis denotes an end, and we assume that all values within a set of parentheses satisfy that particular lifetime: a value is said to \textit{satisfy} a lifetime if that lifetime is a super lifetime of that value's lifetime, i.e.
\begin{equation}
    v \in \alpha = (b, e) \iff \mt{begin}(v) > b \land \mt{end}(v) < e
\end{equation}

Notice our use of the \(\dep\) symbol in Figure \ref{fig:ex1-graph}: as the computation of \(h\) requires the use of \(s\), and when it finishes, \(h\) may then be used (i.e. becomes live), it follows that the beginning of \(h\) must be some unspecified interval of time after the end of \(s\), which must be before the end of \(v\) (since \(s\) borrows from \(v\)). Hence, we represent this by putting the opening \((\) of \(h\) just before the closing \()\) of \(s\), and linking the resulting symbol, \(\dep\), to the  closing of \(v\). Note the distinction between this and the transition between \(u\) and \(v\): since creating \(u\) destroys \(v\), then \textit{first} \(v\) is destroyed, and then (perhaps after some computation, or even blocking) \(u\) is available for use. We will often leave out lifetime annotations where they are trivial: for example, we would usually not draw the parentheses around \(h\), and in diagrams without borrowing, we will often draw no parentheses at all.

\begin{figure}
    \centering
    \tikzfig{graphs/example1-lifetime}
    \caption{
        A temporal RVSDG for the program in Figure \ref{fig:ex1}, implicitly containing the temporal constraints introduced in Figure \ref{fig:ex1-constrained}
        \label{fig:ex1-graph}
    }
\end{figure}

Now that we have a graphical picture of how lifetime restrictions work, let us reinforce our intuition by considering some invalid programs which will be rejected by a lifetime check. Specifically, consider the Rust program in Figure \ref{fig:borrow-after-free}. This function first doubles it's input (a vector) and then attempts to compute the product of the result and the original input vector. This violates the lifetime check as the function \tty{double_vec} consumes the vector \tty{v}, and yet the result \tty{u} and \tty{v} are used in the same expression.

\begin{figure}
    \centering
    \lstinputlisting[language=rust, style=rainstyle]{code/borrow-after-use.rs}
    \caption{
        An invalid program attempting to compute the dot product \(2v \cdot v\), demonstrating a ``use after free'' bug.
        \label{fig:borrow-after-free}
    }
\end{figure}

Drawing an RVSDG for this program, we obtain Figure \ref{fig:graph-borrow-after-free}. We see that, \textit{when we include the temporal edges}, a cycle is introduced: the dot product \(d\) must begin after the end of \(v\) (since it consumes \(u\), which consumes \(v\)) but also before the end of \((v\), since it borrows \(v\): this is a contradiction, yielding an error. Naively, then, we can equate borrow-checking with cycle checking in the resulting RVSDG. However, we will later want to add more complex constraints, e.g. that one point in time is less than or equal to another, which admit cycles; however, the underlying algorithm remains a topological sort.

\begin{figure}
    \centering
    \tikzfig{graphs/borrow-after-use}
    \caption{
        A temporal RVSDG for the program in Figure \ref{fig:borrow-after-free}, showing the cycle induced by the borrow-after-free bug in the code
        \label{fig:graph-borrow-after-free}
    }
\end{figure}

One last semantic issue remains: given a source program in an imperative language, as the programs given so far can be interpreted to be, we may wish to reject programs which \textit{may} be re-ordered into valid programs but, as given, are invalid. To do this, we can simply compose a list of temporal constraints imposed by the control flow of the source imperative program, and add them as edges during the lifetime check (but not to the graph, where they would impede optimizations). One useful side-effect of this method is that we may then easily generate a \textit{valid} ordering of an invalid program to guide the user along, if one exists.

\section{Temporal Simply Typed Lambda Calculus}

Now that we have built an intuition for how instants work on data-dependence graphs, let us consider how they can be built into a type system. We will begin by extending the simply-typed lambda calculus with \textit{instants}, which we use to add Rust-like \cite{rust} lifetime-parametrization. Note we will \textit{not}, for the time being, be using linear or sub-structural logic: in Section \ref{sec:abstract}, we will show how linear logic can be built into the system of instants. We begin by introducing a simple grammar in Figure \ref{fig:simplegrammar}

\begin{figure}
    \centering
    \begin{grammar}
        <term> := <atomic-expr>
        \alt \(\lambda\) <term> . <term>
        \alt <term> <term>

        <type> := <atomic-type>
        \alt <type> \(\to\) <type>
        \alt \& <lifetime> <type>
        \alt \(\forall\) <instant-var> . <type>

        <lifetime> := <atomic-lifetime>
        \alt (<instant>, <instant>)

        <instant> := <atomic-instant>
        \alt <instant> \(\wedge\) <instant>

        <instant-var> := <variable>
        \alt <variable> \(\prec\) <instant>
        \alt <variable> \(\preceq\) <instant>
        \alt <variable> \(\succ\) <instant>
        \alt <variable> \(\succeq\) <instant>
    \end{grammar}
    \caption{
        An EBNF grammar for the simply typed lambda calculus extended with lifetimes.
        \label{fig:simplegrammar}
    }
\end{figure}

We will introduce typing judgements of the form \(x: T\) (``\(x\) is of type \(T\)'') and ordering judgements on instants (e.g. \(a \prec b\): instant \(a\) comes before instant \(b\)).
Already, however, simple terms will be irritating to write: for example, the signature
\begin{lstlisting}[language=rust, autogobble=true, style=rainstyle]
    fn  f<'b: 'a>(x: &'b T) -> &'b R; 
\end{lstlisting}
becomes the term, writing \(a = (a_b, a_e)\) and \(b = (b_b, b_e)\),
\begin{equation}
    f: \forall b_b \preceq a_b . \forall b_e \succeq a_e .\&_{(b_b, b_e)} \ T \to \&_{(b_b, b_e)} R
    \label{eqn:bigsig}
\end{equation}
So, we introduce some abbreviations:
\begin{equation}
    \alpha = (a, b) \supseteq \beta = (c, d) \quad \equiv \quad a \preceq c \land b \succeq d
\end{equation}
Similarly, we extend
\begin{equation}
    \forall \alpha = (a, b) \supseteq \beta = (c, d) . T \quad \equiv \quad \forall a \preceq c .\forall b \succeq d . T
\end{equation}
and likewise for \(\subseteq, \supset, \subset\).
This makes the signature in Equation \ref{eqn:bigsig} into simply
\begin{equation}
    f: \forall \beta \subseteq \alpha . \&_\beta T \to \&_\beta R
\end{equation}
We now, for example, would have derivation
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter f: \forall \beta \supseteq \alpha . \&_\beta T \to R\)}
        \AxiomC{\(\Gamma \fCenter x: \&_\eta T\)}
        \AxiomC{\(\Gamma \fCenter \eta \supseteq \alpha\)}
        \TrinaryInfC{
            \(\Gamma \fCenter fx: \&_\eta R\)
        }
    \end{mathprooftree}
    \label{eqn:figtrin}
\end{equation}
Notice that in Equation \ref{eqn:figtrin}, \(\eta \subseteq \alpha\) is a lifetime constraint which we can automatically generate by trying to substitute \(\eta\) for \(\beta\). In general, however, we can also generate such constraints from arbitrary substitutions between reference types: we begin by noting that reference types are \textit{contravariant} in their lifetime parameter. That is, if a reference \(r\) is valid for lifetime \(\alpha\), and \(\alpha \supseteq \beta\), then obviously \(r\) is valid for lifetime \(\beta\) as well, and hence the longer lifetime yields a subtype of the shorter one. Hence, we introduce the rule
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter x: \&_\alpha T\)}
        \AxiomC{\(\Gamma \fCenter \alpha \supseteq \beta\)}
        \BinaryInfC{\(\Gamma \fCenter x: \&_\beta T\)}
    \end{mathprooftree}
\end{equation}
We can now, for example, have the derivation
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter f: \&_\beta T \to R\)}
        \AxiomC{\(\Gamma \fCenter x: \&_\alpha T\)}
        \AxiomC{\(\Gamma \fCenter \alpha \supseteq \beta\)}
        \BinaryInfC{\(\Gamma \fCenter x: \&_\beta T\)}
        \BinaryInfC{
            \(\Gamma \fCenter fx: R\)
        }
    \end{mathprooftree}
\end{equation}
which we may abbreviate as
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter f: \&_\beta T \to R\)}
        \AxiomC{\(\Gamma \fCenter x: \&_\alpha T\)}
        \AxiomC{\(\Gamma \fCenter \alpha \supseteq \beta\)}
        \TrinaryInfC{
            \(\Gamma \fCenter fx: R\)
        }
    \end{mathprooftree}
    \label{eqn:figtrinref}
\end{equation}
We may also, however, think of this in the following way: we attempt to substitute \(\&_\beta T\) for \(\&_\alpha T\) which requires that \(\&_\beta T\) is a subtype of \(\&_\alpha T\). We know \(\&\) is contravariant, and so this requires that \(\alpha \subseteq \beta\). This immediately suggests an inference algorithm for lifetime constraints, and hence we can write a type checker with the following signature:
\begin{equation}
    \text{Term} \to \{\text{Constraint}\} + \text{Error}
    \label{eqn:conterm}
\end{equation}
In Rust \cite{rust}, lifetimes are determined by program control flow, so this list of constraints must then be checked. However, our language, being purely functional, has a much looser notion of control flow: any re-ordering of the RVSDG which obeys temporal edge constraints may be admitted. Hence, rather than encode temporal edge constraints, and then check the required constraints from Equation \ref{eqn:conterm} above, we may instead \textit{assume} the constraints from Equation \ref{eqn:conterm} are true, along with, perhaps, any additional temporal constraints from the program itself. Our compiler must then simply be constrained to attempt to construct an RVSDG ordering obeying all constraints, which it may do with a simple graph search: if no such ordering is possible, there is a contradiction, and hence in \textit{this} case we can say that the lifetime did not typecheck.

\section{Abstract Terms and Types}

\label{sec:abstract}

One important feature of this kind of design is that the lifetime checking and type-checking phases are thus separate. Hence, in a way, we can think of, for example, \(\&_*T\) as a type, for an unspecified lifetime \(*\), and then after type checking we separately perform a ``lifetime check.'' More interestingly, assume an expression type checks but fails the lifetime check: we may still speculate as to what the result ``would be'' if the lifetime check had instead succeeded. This idea leads us to the concept of an \textbf{abstract term} and \textbf{abstract type}. By generalizing this notion, we also separate sub-structurality checks from the type system, allowing us to ignore the issue of occurences of sub-structural types in type signatures.

We begin by more formally specifying the type-checking algorithm from the previous section by introducing the notion of a \textbf{potential subtype}: a type which is a subtype of another given a unique minimal constraint-set holds. For example, we have that, for any lifetimes \(\alpha, \beta\), \(\&_\alpha T\) is a potential subtype of \(\&_\beta T\), with constraint-set \(\{\alpha \supseteq \beta\}\). For now, we will write this relation as \(\mc{S}(T, U) = C\), where \(C\) is a constraint set. For example, we would write
\begin{equation}
    \Sub(\&_\alpha T, \ \&_\beta T) = \{\alpha \supseteq \beta\}
\end{equation}
To deal with totally different types, e.g. \(\mt{u64}\) and \(\mt{\&'static\ str}\), we will introduce the value \(\bot\), which we will treat like an unsatisfiable constraint set. So, for example, we have
\begin{equation}
    \Sub(\mt{u64}, \ \mt{\&'static\ str}) = \bot
\end{equation}
Then, invalid type substitutions are precisely those which have an unsatisfiable constraint set.

We define the symmetric constraint set, or compatiblity, operator to be given by
\begin{equation}
    \Compat(T, T') = \Sub(T, T') \cup \Sub(T', T)
\end{equation}
where we define \(\bot \cup C = C \cup \bot = \bot\) for any constraint-set \(C\). We call \textit{any} two types with symmetric constraint set not equal to \(\bot\) (even if their constraint set, like \(\{a \prec b, b \prec a\}\), is inconsistent) \textit{compatible}, and will allow equality judgements (and later, identity types) to be formed between any compatible types.

We now proceed to annotate every term with a constraint-set which must hold for that term to be valid. Every term with a consistent constraint set is called \textbf{concrete}; every term with an inconsistent constraint set not equal to \(\bot\) is called \textbf{abstract}: a term with constraint set \(\bot\) simply does not compile at all. 

Note that so far, our constraint sets represent only a restricted form of subtyping judgement in the simply typed lambda calculus, partially because we have not yet introduced \textit{borrowing}. Borrowing, however, really only makes sense in the setting of sub-structural types. What's interesting is we can now frame not only temporal rules, but also \textbf{substructurality rules}, in terms of a constraint set. Recall that a substructural term is either
\begin{itemize}
    \item Affine, i.e. can only be used once
    \item Relevant, i.e. must be used once
    \item Linear, i.e. must be used exactly once. This is equivalent to being both affine and relevant
\end{itemize}
We may hence extend our notion of constraint-set with the following constraints:
\begin{itemize}
    \item An introduction constraint, implying a variable \(v\) has been introduced.
    \item A usage constraint, implying a variable \(v\) has been used.
\end{itemize}
We will annotate each individual introduction/usage constraint with a unique label \(\ell\). An affine term is hence merely a term for which having two different usage constraints is an error, while a relevant term is one for which having an introduction constraint without a usage constraint is an error: we can detect the presence of multiple usages by comparing labels. In particular, we note that sub-structurality now only determines whether a term is abstract or concrete: terms may be constructed which e.g. use a linear term twice, but such terms will be considered abstract.

\section{Regions and Borrowing}

Now that we've defined the basic setting of simple temporal type theory, we need to determine how to handle the other important detail in the RVSDGs from Section \ref{sec:rvsdg}: borrowing. To do so, we must define the \textit{lifetime} of an individual value. Intuitively, a value's lifetime is the span of time it is ``live,'' i.e. can be used in other values. For a value on the stack, it's lifetime begins when it is pushed onto the stack and ends when it is popped; for a value on the heap, it's lifetime begins when it is allocated and ends when it is freed. In the abstract, then, it is very easy to define a term \(t\)'s lifetime: just assign it a beginning instant \(b(t)\) and an end instant \(e(t)\) (ignoring compound terms like \tty{struct}s for now) and constrain these as appropriate.

For example, consider a stack-allocated term \(E: T\) which depends on free variables \(x, y, z\). In the general case, \(E\)'s end should then be constrained to be before the end of each of \(x, y, z\), i.e. before the stack frame \(E\) would trivially be put in is popped. Then, a borrow of \(E\), say the expression \(\&E\), can simply be given type \(\&_{(b(E), e(E))}T\). So, writing the free variable set of an expression as \(\fv\) and the minimum of two instants as \(\wedge\), we would require
\begin{equation}
    e(E) \preceq \bigwedge_{v \in \fv(E)}e(v) \iff \forall v \in \fv(E), e(E) \preceq e(v)
\end{equation}
The interesting part, however, comes in the case of return values. In particular, we do \textit{not} want to return a value which must be destroyed before the end of the current stack frame, for example, a dangling pointer to a slot in this stack frame. This prevents classic errors, like that in Figure \ref{fig:return-borrow}, but the issue is it wouldn't allow us to return a stack variable at all. A natural solution, then, is to \textit{instead} place a stack frame constraint on \textit{borrows} only: i.e. have
\begin{equation}
    e(\&E) \preceq \bigwedge_{v \in \fv(E)}e(v) \iff \forall v \in \fv(E), e(E) \preceq e(v)
\end{equation}
along with, of course, \(e(\&E) \preceq e(E)\). The nice part is is this allows a trivial compilation into LLVM: \(\&E\) is equivalent to an \tty{alloca} instruction where the result of the virtual register \(E\) is copied onto the stack. We call the free variable set a value depends on it's \textbf{region}, and call a lifetime bounded by a free variable set \textbf{region-bounded}. This corresponds to the RVSDG's notion of regions \cite{rvsdg}, but is generalized froma tree of regions into a DAG of regions generated by the inclusion relation.

\begin{figure}
    \lstinputlisting[language=rust, style=rainstyle]{code/return-borrow.rs}
    \caption{
        Examples of correctly and incorrectly returning borrowed values
        \label{fig:return-borrow}
    }
\end{figure}

\section{Type Polymorphism and Dependent Function Types}

Now that we have explored the basics of the simply-typed temporal lambda calculus, we can begin to try expanding it with a notion of \textit{type polymorphism}, as in a Hindley-Milner \cite{milner} type system. One complication is we must distinguish between types which \textit{may be} affine (but not relevant), types which \textit{may be} relevant (but not affine), and types which may be both, i.e. linear. It is also helpful to be able to have polymorphism over, e.g., ``all types whose members live longer than a lifetime \(\alpha\).'' To do so, we will skip straight to a kind of dependent typing, and introduce \textit{typing universes}, which we will denote as
\begin{equation}
    \forall i \in \nats, \mc{U}_\alpha^C(i)
\end{equation}
where \(C \in \{U, A, R, L\}\) for unrestricted, affine, relevant, and linear respectively. We parametrize universes by a level \(i \in \nats\) to avoid Girard's paradox, which would be triggered if we had \(\mc{U} : \mc{U}\) for any universe \(\mc{U}\). We instead have
\begin{equation}
    \mc{U}_\alpha^C(i) : \mc{U}_\alpha^C(i + 1)
\end{equation}
and introduce subtyping judgements
\begin{equation}
    \Sub(\mc{U}_\alpha^C(i), \mc{U}_\beta^D(i)) = \{C \implies D, \alpha \supseteq \beta\}
\end{equation}
where we have partial order \(L \implies R, A \implies U\). Note we do not handle the case of universe subtyping where \(i \leq j\) here: this is because a term which fails such a constraint is not merely abstract, but actually \textit{invalid} (as otherwise, an abstract term could encode Girard's paradox, but we want such terms to remain consistent).

We define universes to be closed under function types, i.e.
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter A: \mc{U}\)}
        \AxiomC{\(\Gamma \fCenter B: \mc{U}\)}
        \BinaryInfC{\(\Gamma \fCenter A \to B: \mc{U}\)}        
    \end{mathprooftree}
\end{equation}
To be able to define type polymorphic functions, then, we must introduce the dependent function type \(\prod\), with the following formation rule:
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma, x: A \fCenter T: \mc{U}\)}
        \UnaryInfC{\(\Gamma \fCenter \prod_{x: A}T: \mc{U}\)}        
    \end{mathprooftree}
\end{equation}
We then generalize lambda functions to be able to generate terms of the dependent function type
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma, x: A \fCenter E: T\)}
        \UnaryInfC{\(\Gamma \fCenter \lambda x. E : \prod_{x: A}T\)}    
    \end{mathprooftree}
\end{equation}
and correspondingly generalize the elimination rule to
\begin{equation}
    \begin{mathprooftree}
        \AxiomC{\(\Gamma \fCenter f: \prod_{x: A}B(x)\)}
        \AxiomC{\(\Gamma \fCenter a: A\)}
        \BinaryInfC{\(\Gamma \fCenter fa : B(a)\)}    
    \end{mathprooftree}
\end{equation}
The important thing to keep in mind here is that the constraint-set of the \textit{type} is ignored, and in particular does not affect the constraint set of the \textit{term}. This achieves the desired separation of intuitionistic terms from linear ones, avoiding the problem of linear terms appearing within a type. Similarly, the issue of temporally constrained terms appearing within a type is also overcome. In particular, a type-generic function may be abstract, but an \textit{application} of this function may be concrete, as the constraint-set is computed on the underlying application.

\newpage

\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}